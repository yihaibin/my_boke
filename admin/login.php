<?php
include 'common.php';
if ($user->hasLogin()) {
    $response->redirect($options->adminUrl);
}
$rememberName = htmlspecialchars(Typecho_Cookie::get('__typecho_remember_name'));
Typecho_Cookie::delete('__typecho_remember_name');
$bodyClass = 'body-100';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>登录后台</title>
<link rel="stylesheet" type="text/css" href="css/style_log.css"/>
</head>

<body>
    <div class="bg">
    <form action="<?php $options->loginAction(); ?>" method="post" name="login" role="form">
            <div class="bg_2">
            <div class="hhh">
                <p class="hai">登录</p>
                <div class="search">
                        <form  method="post" target="_blank" name="formsearch" action="/plus/search.php">
                        <div class="search_box">
                           <input type="text" id="name" name="name" value="<?php echo $rememberName; ?>" value="用户名" onfocus="if(this.value=='用户名'){this.value='';}"  onblur="if(this.value==''){this.value='用户名';}" class="text-l w-100" autofocus />
                             <input type="password" id="password" name="password" class="text-l w-100" value="密码" onfocus="if(this.value=='密码'){this.value='';}"  onblur="if(this.value==''){this.value='密码';}" />
                                <button type="submit" class="search_button"> 登陆</span></button>
                        </div>
                        </form>
                </div>
            </div>
            </div>
    </div>
    </form>
</body>
