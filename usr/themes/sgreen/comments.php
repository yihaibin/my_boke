<div id="comments">

    <div class="col-md-12 col-sm-9">
        <?php $this->comments()->to($comments); ?>
        <?php if ($comments->have()): ?>
        <div class="comment-title">
            <h3><?php $this->commentsNum(_t('暂无评论'), _t('仅有 1 条评论'), _t('已有 %d 条评论')); ?></h3>
        </div>
        <?php $comments->listComments(); ?>
        <?php $comments->pageNav('&laquo; 前一页', '后一页 &raquo;'); ?>
        <?php endif; ?>

        <?php if($this->allow('comment')): ?>
        <div id="<?php $this->respondId(); ?>" class="respond">
            <div class="cancel-comment-reply">
                <?php $comments->cancelReply(); ?>
            </div>

            <div class="panel panel-sgreen">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php _e('添加新评论'); ?></h3>
                </div>
                <div class="panel-body">
                    <form method="post" action="<?php $this->commentUrl() ?>" id="comment-form" class="form-inline" role="form">
                        <?php if($this->user->hasLogin()): ?>
                        <p>
                            <?php _e('登录身份：'); ?>
                            <a href="<?php $this->options->profileUrl(); ?>"><?php $this->user->screenName(); ?></a>
                            .
                            <a href="<?php $this->options->logoutUrl(); ?>" title="Logout">
                                <?php _e('退出'); ?>&raquo;</a>
                        </p>
                        <?php else: ?>
                        <p>
                            <div class="form-group">
                                <label for="author" class="required">
                                    <?php _e('称呼'); ?></label>
                                <input type="text" name="author" id="author" class="form-control" placeholder="<?php _e('输入称呼'); ?>" value="<?php $this->remember('author'); ?>" />
                            </div>
                            <div class="form-group">
                                <label for="mail"<?php if ($this->options->commentsRequireMail): ?> class="required"<?php endif; ?>><?php _e('Email'); ?></label>
                                <input type="email" name="mail" id="mail" class="form-control" placeholder="<?php _e('输入邮箱'); ?>" value="<?php $this->remember('mail'); ?>" />
                            </div>
                            <div class="form-group">
                                <label for="url"<?php if ($this->options->commentsRequireURL): ?> class="required"<?php endif; ?>><?php _e('网站'); ?></label>
                                <input type="url" name="url" id="url" class="form-control" placeholder="<?php _e('http://example.com'); ?>" value="<?php $this->remember('url'); ?>" />
                            </div>
                        </p>

                        <?php endif; ?>
                        <p>
                        <textarea cols="85" rows="5" placeholder="<?php _e('你的评论也许能一针见血！'); ?>" name="text" class="form-control"><?php $this->remember('text'); ?></textarea>
                        </p>
                        <p>
                            <button type="submit" class="btn btn-sgreen"><?php _e('提交评论'); ?></button>
                        </p>
                    </form>
            </div>
            <?php else: ?>
            <h3><?php _e('评论已关闭'); ?></h3>
            <?php endif; ?></div>
    </div>
</div>

</div>