      <!-- breadcrumb -->      
      <div class="col-md-12 col-sm-9">
        <ol class="breadcrumb">
          你的位置：
          <li>
            <span class="glyphicon glyphicon-home"></span><a href="<?php $this->options->siteUrl(); ?>"> 首页</a>
          </li>
          <?php if ($this->is('post')): ?>
          <li>
            <span class="glyphicon glyphicon-file"></span> <?php $this->category(); ?>
          </li>
          <li class="active">
            <?php $this->title() ?>
          </li>
          <?php else: ?>      
          <li class="active">
            <?php $this->archiveTitle('','',''); ?>
          </li>
          <?php endif; ?>
        </ol>
      </div>