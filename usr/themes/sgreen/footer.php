 <footer>
     <div class="footer">
        &copy;<?php echo date('Y'); ?> <a href="<?php $this->options->siteUrl(); ?>"><?php $this->options->title(); ?></a>.
        <?php _e('Powered by <a href="http://www.typecho.org">Typecho</a>'); ?>.
        <?php _e('Theme by <a href="http://www.xliboy.com">xliboy</a>'); ?>.
    </div>
 </footer>
 </div>
 <!--/.container--> 

 <!-- Include all compiled plugins (below), or include individual files as needed --> 
 <script src="http://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
 <script src="http://cdn.bootcss.com/twitter-bootstrap/3.0.2/js/bootstrap.min.js"></script>
 <script src="<?php $this->options->themeUrl('js/jquery.scrollUp.min.js'); ?>"></script>
 <script type="text/javascript">
    $('a').tooltip('hide');
    $.scrollUp({
        scrollText: '回到顶部',
        scrollImg: true,
    });
    </script>

 </body>
 </html>