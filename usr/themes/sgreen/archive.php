<?php $this->need('header.php'); ?>
<div class="row row-offcanvas row-offcanvas-right">
  <div class="col-xs-12 col-sm-9">
    <div class="row">

      <?php $this->need('breadcrumb.php'); ?>
      <?php while($this->next()): ?>
      <div class="col-md-12 col-sm-9">
        <div class="panel panel-default">
            <div class="post-title">
              <h3><a href="<?php $this->permalink() ?>"><span class="glyphicon glyphicon-chevron-right"></span><?php $this->title() ?></a></h3>
            </div>
          <div class="panel-body">
            <?php if (empty($this->options->sidebarBlock) || in_array('ShowThumbnail', $this->options->sidebarBlock)): ?>
            <div class="thumb pull-left">
              <a href="<?php $this->permalink() ?>" class="thumbnail">
                <img src="<?php $this->options->themeUrl('thumb/'); ?><?php $this->theId() ?>.jpg" style="width: 200px;height: 150px;" title="<?php $this->title() ?>" alt="<?php $this->title() ?>"></a>
            </div>
            <?php endif; ?>
            <?php $this->excerpt(350, '...'); ?></div>
          <div class="panel-footer">
            <button class="btn btn-default btn-sm"><span class="glyphicon glyphicon-file"></span> <?php $this->category(','); ?></button>
            <button class="btn btn-default btn-sm"><a href="<?php $this->permalink() ?>#comments"><span class="glyphicon glyphicon-comment"></span> <?php $this->commentsNum('评论', '1 条评论', '%d 条评论'); ?></a></button>
            <button class="btn btn-default btn-sm" disabled="disabled"><span class="glyphicon glyphicon-calendar"></span> <?php $this->date('Y-m-d'); ?></button>

            <div class="readmore pull-right">
              <a href="<?php $this->permalink() ?>" class="btn btn-sgreen" role="button">阅读更多</a>
            </div>
          </div>
        </div>
      </div>
      <!--/span-->
      <?php endwhile; ?>

      <div class="col-md-12 col-sm-9"><?php $this->pageNav('&laquo; 前一页', '后一页 &raquo;'); ?></div>

      <!--/span--> </div>
    <!--/row--> </div>
  <!--/span-->

  <?php $this->need('sidebar.php'); ?>
  <?php $this->need('footer.php'); ?>