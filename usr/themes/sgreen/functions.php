<?php

function themeConfig($form) {
    $topNotice = new Typecho_Widget_Helper_Form_Element_Text('topNotice', NULL, NULL, _t('顶部公告'), _t('这里可以输入一段文字显示顶部公告，不填则显示随机名言。'));
    $form->addInput($topNotice);
    $weibo = new Typecho_Widget_Helper_Form_Element_Text('weibo', NULL, NULL, _t('微博地址'), _t('这里输入你的微博地址,不填则不显示。'));
    $form->addInput($weibo);
    
    $sidebarBlock = new Typecho_Widget_Helper_Form_Element_Checkbox('sidebarBlock', 
    array('ShowThumbnail' => _t('显示文章缩略图（位于网站根目录下thumb/post-id.jpg）'),
    'ShowTopBanner' => _t('显示顶部banner'),
    'ShowAboutAuthor' => _t('显示关于作者'),
    'ShowRecentPosts' => _t('显示最新文章'),
    'ShowRecentComments' => _t('显示最近回复'),
    'ShowCategory' => _t('显示分类'),
    'ShowArchive' => _t('显示归档'),
    'ShowOther' => _t('显示其它杂项')),
    array('ShowTopBanner','ShowAboutAuthor','ShowRecentPosts', 'ShowRecentComments', 'ShowCategory', 'ShowArchive', 'ShowOther'), _t('主题设置'));
    
    $form->addInput($sidebarBlock->multiMode());
}