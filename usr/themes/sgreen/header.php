<!DOCTYPE HTML>
<head>
<meta charset="<?php $this->
options->charset(); ?>" />
<title><?php $this->archiveTitle(' &raquo; ', '', ' - '); ?><?php $this->options->title(); ?></title>

<link rel="stylesheet" href="http://cdn.bootcss.com/twitter-bootstrap/3.0.2/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php $this->options->themeUrl('style.css'); ?>" />
<link rel="apple-touch-icon-precomposed" sizes="128x128" href="<?php $this->options->themeUrl('images/icon.png'); ?>">
<link rel="shortcut icon" href="<?php $this->options->themeUrl('images/favicon.png'); ?>">
<!--[if lt IE 9]>
<script src="http://cdn.bootcss.com/html5shiv/3.7.0/html5shiv.min.js"></script>
<script src="http://cdn.bootcss.com/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<?php $this->header(); ?>
</head>
<body>

<div class="container">
    <div id="header">
        <div class="social-icon pull-right">
            <?php if ($this->
            options->weibo): ?>
            <a href="<?php $this->options->weibo() ?>" target="_blank" class="weibo text-hide" data-toggle="tooltip" data-placement="auto" title="关注我的微博">微博</a>
            <?php endif; ?></div>
        <div class="random">
            <?php if ($this->options->topNotice): ?>
            <?php $this->options->topNotice() ?>
            <?php else: ?>
            <script src="<?php $this->options->themeUrl('js/random.js'); ?>"></script>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="marbr"></div>
<div class="container">
    <!-- logo -->
    <div class="row">
        <div class="col-md-4">
            <div class="logo pull-left">
                <a href="<?php $this->options->siteUrl(); ?>" class="text-hide"><?php $this->options->title() ?></a>
            </div>
        </div>

        <div class="col-md-8">
            <div class="top-banner pull-right">
                <?php if (empty($this->options->sidebarBlock) || in_array('ShowTopBanner', $this->options->sidebarBlock)): ?>
                <?php $this->need('banner.php'); ?>
                <?php endif; ?>
            </div>
        </div>

    </div>

    <!-- nav -->
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li<?php if($this->is('index')): ?> class="active"<?php endif; ?>>
                    <a href="<?php $this->options->siteUrl(); ?>" title="首页"><span class="glyphicon glyphicon-home"></span><?php _e(' 首页'); ?></a>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="所有分类"><span class="glyphicon glyphicon-plus"></span> 所有分类</a>
                    <ul class="dropdown-menu">
                        <?php $this->widget('Widget_Metas_Category_List')->parse('<li><a href="{permalink}">{name}</a></li>'); ?>
                    </ul>
                </li>

                <?php $this->widget('Widget_Contents_Page_List')->to($pages); ?>
                <?php while($pages->next()): ?>
                <li<?php if($this->is('page', $pages->slug)): ?> class="active"<?php endif; ?>>
                    <a href="<?php $pages->permalink(); ?>" title="<?php $pages->title(); ?>"><?php $pages->title(); ?></a>
                </li>
                <?php endwhile; ?>
            </ul>
            <form class="navbar-form navbar-right" role="search" id="search" method="post" action="./">
                <div class="form-group">
                    <input type="text" name="s" class="form-control" placeholder="<?php _e('输入关键字搜索'); ?>">
                </div>
                <button type="submit" class="btn btn-sgreen"><?php _e('搜索'); ?></button>
            </form>

        </div>
        <!-- /.navbar-collapse --> </nav>
    <!-- nav end -->