<div class="col-xs-6 col-sm-3 sidebar-offcanvas hidden-xs" id="sidebar" role="navigation">

  <!-- 其他 -->
  <?php if (empty($this->options->sidebarBlock) || in_array('ShowOther', $this->options->sidebarBlock)): ?>
  <div class="panel panel-sgreen">
    <div class="panel-heading">
      <h3 class="panel-title"><?php _e('其他'); ?></h3>
    </div>
    <ul class="list-group">
      <?php if($this->user->hasLogin()): ?>
      <li class="list-group-item"><a href="<?php $this->options->adminUrl(); ?>"><?php _e('进入后台'); ?>(<?php $this->user->screenName(); ?>)</a></li>
      <li class="list-group-item"><a href="<?php $this->options->logoutUrl(); ?>"><?php _e('退出'); ?></a></li>
      <?php else: ?>
      <li class="list-group-item"><a href="<?php $this->options->adminUrl('login.php'); ?>"><?php _e('登录'); ?></a></li>
      <?php endif; ?>
    </ul>
  </div>
  <?php endif; ?>

  <!-- 最新文章 -->
  <?php if (empty($this->options->sidebarBlock) || in_array('ShowRecentPosts', $this->options->sidebarBlock)): ?>
  <div class="panel panel-sgreen">
    <div class="panel-heading">
      <h3 class="panel-title"><?php _e('最新文章'); ?></h3>
    </div>
    <ul class="list-group">
      <?php $this->widget('Widget_Contents_Post_Recent')->parse('<li class="list-group-item"><a href="{permalink}">{title}</a></li>'); ?>
    </ul>
  </div>
  <?php endif; ?>

  <!-- 最新回复 -->
  <?php if (empty($this->options->sidebarBlock) || in_array('ShowRecentComments', $this->options->sidebarBlock)): ?>
  <div class="panel panel-sgreen">
    <div class="panel-heading">
      <h3 class="panel-title"><?php _e('最近回复'); ?></h3>
    </div>
    <ul class="list-group">
      <?php $this->widget('Widget_Comments_Recent')->to($comments); ?><?php while($comments->next()): ?>
      <li class="list-group-item"><a href="<?php $comments->permalink(); ?>"><?php $comments->author(false); ?></a>：<?php $comments->excerpt(35, '...'); ?></li><?php endwhile; ?></ul>
  </div>
  <?php endif; ?>

  <!-- 分类 -->
  <?php if (empty($this->options->sidebarBlock) || in_array('ShowCategory', $this->options->sidebarBlock)): ?>
  <div class="panel panel-sgreen">
    <div class="panel-heading">
      <h3 class="panel-title"><?php _e('分类'); ?></h3>
    </div>
    <ul class="list-group">
      <?php $this->widget('Widget_Metas_Category_List')->parse('<li class="list-group-item"><a href="{permalink}">{name}</a>({count})</li>'); ?>
    </ul>
  </div>
  <?php endif; ?>
  <div class="panel panel-sgreen">
    <div class="panel-heading">
      <h3 class="panel-title"><?php _e('友情链接'); ?></h3>
    </div>
    <ul class="list-group">
      <li class="list-group-item"><?php Links_Plugin::output(); ?></li>
    </ul>
     
  </div>
  <!--/span-->
</div>
<!--/row-->
<!-- sidebar end -->
</div>