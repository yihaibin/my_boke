<?php $this->
need('header.php'); ?>
<div class="row row-offcanvas row-offcanvas-right">
  <div class="col-xs-12 col-sm-9">
    <div class="row">

      <?php $this->need('breadcrumb.php'); ?>
      <!-- post cantant -->
      <div class="col-md-12 col-sm-9">
        <div class="panel panel-default">

          <div class="post-title">
            <h3><span class="glyphicon glyphicon-chevron-right"></span><?php $this->title() ?></h3>
          </div>

          <div class="panel-body">

            <?php $this->content(); ?>
            <hr>
            <p>原文链接：<?php $this->permalink() ?></p>
          </div>
          <div class="panel-footer">
            <div class="bshare-custom icon-medium-plus">
              <a title="分享到QQ空间" class="bshare-qzone"></a>
              <a title="分享到新浪微博" class="bshare-sinaminiblog"></a>
              <a title="分享到人人网" class="bshare-renren"></a>
              <a title="分享到腾讯微博" class="bshare-qqmb"></a>
              <a title="分享到网易微博" class="bshare-neteasemb"></a>
              <a title="更多平台" class="bshare-more bshare-more-icon more-style-addthis"></a>
              <span class="BSHARE_COUNT bshare-share-count">0</span>
            </div>
            <script type="text/javascript" charset="utf-8" src="http://static.bshare.cn/b/buttonLite.js#style=-1&amp;uuid=&amp;pophcol=2&amp;lang=zh"></script>
            <script type="text/javascript" charset="utf-8" src="http://static.bshare.cn/b/bshareC0.js"></script>
          </div>
        </div>
      </div>
      <!--/span-->

      <?php $this->need('comments.php'); ?>
      <!--/span--> </div>
    <!--/row--> </div>

  <?php $this->need('sidebar.php'); ?>
  <?php $this->need('footer.php'); ?>