<!DOCTYPE HTML>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="<?php $this->options->charset(); ?>" />
	<meta name="viewport" content="width=device-width,user-scalable=no">
    <title><?php $this->archiveTitle(array(
            'category'  =>  _t('分类 %s 下的文章'),
            'search'    =>  _t('包含关键字 %s 的文章'),
            'tag'       =>  _t('标签 %s 下的文章'),
            'author'    =>  _t('%s 发布的文章')
        ), '', ' - '); ?><?php $this->options->title(); ?></title>
	<!--[if lt IE 9]>
    <![endif]-->

   <link rel="stylesheet" href="<?php $this->options->adminUrl('css/normalize.css'); ?>">
    <link rel="stylesheet" href="<?php $this->options->themeUrl('style.css'); ?>" 

    <?php $this->header("generator=&template=&"); ?>
    <link rel="shortcut icon" href="haibin.png"/>
</head>
<body>

<header id="header" class="clearfix hidden-sm hidden-xs">
    <div class="container hidden-sm hidden-xs">
        <div class="col-group">
            <div class="site-name ">
                <div style=" float: left">
                    <img width="120" height="120" class="profile-avatar" src="http://www.gravatar.com/avatar/cbcc433d2f398142a1fff8ec0cf4ac30?s=220&r=X&d=mm" alt="admin">
                </div>    
                <div style=" float: left; padding-top: 20px; padding-left: 10px;">
                    <a id="logo" href="<?php $this->options->siteUrl(); ?>">
                       <?php $this->options->title() ?>
                    </a>
                    <p class="description"><?php $this->options->description() ?></p>                    
                </div>
                <div>
                    <nav id="nav-menu" class="clearfix" style=" padding-top: 80px">
                        <a class="current" href="<?php $this->options->siteUrl(); ?>"><?php _e('博客'); ?></a>
                        <?php $this->widget('Widget_Contents_Page_List')->to($pages); ?>
                    </nav>
                </div> 
            </div>
        </div>
    </div>
</header>
<div id="body">
    <div class="container">
        <div class="col-group">