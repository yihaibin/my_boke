<?php
/**
 * 简单的响应式模板
 * 
 * @package Maupassant
 * @author cho
 * @version 1.0
 * @link http://pagecho.com
 */
 
 $this->need('header.php');
 ?>

<div class="col-8" id="main">
	<div class="res-cons">
		<?php while($this->next()): ?>
			<article class="post">
				<h2 class="post-title">
					<a href="<?php $this->permalink() ?>"><?php $this->title() ?></a>
				</h2>
				<ul class="post-meta">
					<li><?php $this->date('Y-m-d'); ?></li>
					<li class="comment-count"><a rel="nofollow" href="<?php $this->permalink() ?>#comments"><?php $this->commentsNum('0 条评论', '1 条评论', '%d 条评论'); ?></a></li>
				</ul>
                                <div class="post-content">
                                  <p><?php $this->excerpt(); ?></p>
<p class="more"><a href="<?php $this->permalink() ?>" rel="bookmark" title="<?php $this->title() ?>">-阅读全文-</a></p>
                                </div>
			</article>
		<?php endwhile; ?>
		<?php $this->pageNav('&laquo; Previous','Next &raquo;',10,'...');?>
	</div>
</div>
<?php $this->need('sidebar.php'); ?>
<?php $this->need('footer.php'); ?>
