<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<?php $this->need('header.php'); ?>
<div class="col-md-9" id="page">
	<div class="panel panel-default">
		<div class="panel-heading"><?php $this->title() ?></div>
			<div class="panel-body"><p><span class="glyphicon glyphicon-calendar"></span> <time datetime="<?php $this->date('c'); ?>" itemprop="datePublished"><?php $this->date('F j, Y'); ?></time>&nbsp;&nbsp;&nbsp;&nbsp;
			<!--<span class="glyphicon glyphicon-eye-open"></span><?php Views_Plugin::theViews(); ?> &nbsp;&nbsp;&nbsp;&nbsp;-->
			<span class="glyphicon glyphicon-edit"></span> <a href="<?php $this->permalink() ?>#comments"><?php $this->commentsNum('评论', '1 条评论', '%d 条评论'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-user"></span><a itemprop="name" href="<?php $this->author->permalink(); ?>" rel="author"><?php $this->author(); ?></a></p>
			<p><?php $this->content(); ?></p>
		</div>
		<div class="panel-footer">
			<?php _e('标签: '); ?><?php $this->tags(', ', true, 'none'); ?>
		</div>
</div>
<?php $this->need('comments.php'); ?>
<?php $this->need('sidebar.php'); ?>
<?php $this->need('footer.php'); ?>