<?php
/**
 * 这是 Typecho 1.0 系统的一套bootstrap皮肤
 * 
 * @package Typecho Bootstrap Theme 
 * @author fomde
 * @version 1.0
 * @link http://fomde.com
 */

if (!defined('__TYPECHO_ROOT_DIR__')) exit;
 $this->need('header.php');
 ?>
		<!-- start #main-->
		<div class="col-md-9" id="index">
			<?php while($this->next()): ?>
			<div class="page-header">
				<h3><a href="<?php $this->permalink() ?>"><?php $this->title() ?></a></h3>
				<h6 class="hidden-xs">
					<span class="glyphicon glyphicon-calendar"></span><?php $this->date('F j, Y'); ?>&nbsp;&nbsp;&nbsp;&nbsp;
					<span class="glyphicon glyphicon-th-list"></span> <a href=""><?php $this->category(','); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
					<!--<span class="glyphicon glyphicon-eye-open"></span><?php Views_Plugin::theViews(); ?> &nbsp;&nbsp;&nbsp;&nbsp;-->
					<span class="glyphicon glyphicon-edit"></span> <a href="<?php $this->permalink() ?>#comments"><?php $this->commentsNum('评论', '1 条评论', '%d 条评论'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
					<span class="glyphicon glyphicon-user"></span> <a href="<?php $this->author->permalink(); ?>"><?php $this->author(); ?></a>
				</h6>
				<p class="lead"><?php $this->content('- 阅读剩余部分 -'); ?></p>
			</div>
			<?php endwhile; ?>

			<ol class="page-navigator">
				<?php $this->pageNav('&laquo;', '&raquo;'); ?>
			</ol>
		</div><!-- end #main-->

<?php $this->need('sidebar.php'); ?>
<?php $this->need('footer.php'); ?>