<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<?php $this->need('header.php'); ?>

    <div class="col-md-9" id="post">
        <div class="post">
            <div class="alert alert-info" role="alert">
	    <p><span class="glyphicon glyphicon-th-list"></span>
	    <?php $this->archiveTitle(array(
                'category'  =>  _t('分类 %s 下的文章'),
                'search'    =>  _t('包含关键字 %s 的文章'),
                'tag'       =>  _t('标签 %s 下的文章'),
                'author'    =>  _t('%s 发布的文章')
            ), '', ''); ?></p>
           </div>
        </div>

        <?php if ($this->have()): ?>
    	<?php while($this->next()): ?>
            <div class="post" >
				<div class="page-header">
					<h3><a href="<?php $this->permalink() ?>"><?php $this->title() ?></a></h3>
					<h6 class="hidden-xs">
						<span class="glyphicon glyphicon-calendar"></span><?php $this->date('F j, Y'); ?>&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="glyphicon glyphicon-th-list"></span> <a href=""><?php $this->category(','); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="glyphicon glyphicon-eye-open"></span> &nbsp;&nbsp;&nbsp;&nbsp;
						<span class="glyphicon glyphicon-edit"></span> <a href="<?php $this->permalink() ?>#comments"><?php $this->commentsNum('评论', '1 条评论', '%d 条评论'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="glyphicon glyphicon-user"></span> <a href="<?php $this->author->permalink(); ?>"><?php $this->author(); ?></a></h6>
				</div>
				<div class="entry-content"><?php $this->content('- 阅读剩余部分 -'); ?></div>
			</div><hr>
			<?php endwhile; ?>
        <?php else: ?>
            <div class="post" >
                <h2 class="post-title"><?php _e('没有找到内容'); ?></h2>
            </div>
        <?php endif; ?>

			<ol class="page-navigator">
				<?php $this->pageNav('&laquo;', '&raquo;'); ?>
			</ol>
    </div><!-- end #main -->

	<?php $this->need('sidebar.php'); ?>
	<?php $this->need('footer.php'); ?>