<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="<?php $this->options->charset(); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php $this->archiveTitle(array(
            'category'  =>  _t('分类 %s 下的文章'),
            'search'    =>  _t('包含关键字 %s 的文章'),
            'tag'       =>  _t('标签 %s 下的文章'),
            'author'    =>  _t('%s 发布的文章')
        ), '', ' - '); ?><?php $this->options->title(); ?>（fomde）|品质因素决定一切!</title>

    <!-- Bootstrap -->
    <link href="http://cdn.bootcss.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php $this->options->themeUrl('style.css'); ?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<!-- 通过自有函数输出HTML头部信息 -->
    <?php $this->header('generator=&template=&pingback=&xmlrpc=&wlw=&rss1=&rss2=&atom=&commentReply='); ?>
  </head>
<body>
<!--[if lt IE 8]>
    <div class="browsehappy" role="dialog"><?php _e('当前网页 <strong>不支持</strong> 你正在使用的浏览器. 为了正常的访问, 请 <a href="http://browsehappy.com/">升级你的浏览器</a>'); ?>.</div>
<![endif]-->
<header class="navbar navbar-default navbar-inner navbar-fixed-top" role="navigation">
      <!-- Fixed navbar -->
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php $this->options->siteUrl(); ?>" title="<?php _e('品质因素决定一切&#13;Figure of merit determines everything！'); ?>"><span class="glyphicon glyphicon-th-large"></span>&nbsp;<?php $this->options->title() ?></a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">				
				<li<?php if($this->is('index')): ?> class="active"<?php endif; ?>><a href="<?php $this->options->siteUrl(); ?>"><?php _e('首页'); ?></a></li>
				<li class="dropdown">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php _e('分类'); ?><span class="caret"></span></a>
				  <ul class="dropdown-menu" role="menu">
					<?php $this->widget('Widget_Metas_Category_List')->to($category); ?>
					<?php while ($category->next()): ?>
					<li<?php if ($this->is('post')): ?><?php if ($this->category == $category->slug): ?><?php endif; ?><?php else: ?><?php if ($this->is('category', $category->slug)): ?><?php endif; ?><?php endif; ?>><a href="<?php $category->permalink(); ?>" title="<?php $category->name(); ?>"><?php $category->name(); ?></a></li>
					<?php endwhile; ?>
				  </ul>
				</li>

				<?php $this->widget('Widget_Contents_Page_List')->to($pages); ?>
				<?php while($pages->next()): ?>
				<li<?php if($this->is('page', $pages->slug)): ?> class="active"<?php endif; ?>><a href="<?php $pages->permalink(); ?>" title="<?php $pages->title(); ?>"><?php $pages->title(); ?></a><li>
				<?php endwhile; ?>
				<li class="dropdown">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php if($this->user->hasLogin()): ?><?php $this->user->screenName(); ?><?php else: ?><?php _e('登录'); ?><?php endif; ?><span class="caret"></span></a>
				  <ul class="dropdown-menu" role="menu">
					<?php if($this->user->hasLogin()): ?>
					<li><a href="<?php $this->options->adminUrl(); ?>"><?php _e('进入后台'); ?> (<?php $this->user->screenName(); ?>)</a></li>
					<li><a href="<?php $this->options->adminUrl(); ?>write-post.php"><?php _e('编写文章'); ?> </a></li>
					<li><a href="<?php $this->options->logoutUrl(); ?>"><?php _e('退出'); ?></a></li>
				<?php else: ?>
					<li><a href="<?php $this->options->adminUrl('login.php'); ?>"><?php _e('登录'); ?></a></li>
				<?php endif; ?>
				  </ul>
				</li>
	        </ul>
			<form method="post" action="./" role="search" class="navbar-form navbar-right">
				<div class="input-group">
				<input type="text" name="s" class="form-control" required="true" title="<?php _e('输入关键字搜索'); ?>" placeholder="<?php _e('输入关键字搜索'); ?>">
				<span class="input-group-btn">
				<button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
				</span>
				</div><!-- /input-group -->
			</form>
		  <ul class="nav navbar-nav navbar-right"> 
          </ul>
          </div><!--/.nav-collapse -->
        </div>
</header>

<div class="container">
	<div class="row">