<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
		<div class="col-md-3" id="sidebar"><!-- start #sidebar -->
			<?php if (!empty($this->options->sidebarBlock) && in_array('ShowRecentPosts', $this->options->sidebarBlock)): ?>
			<aside class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title"><span class="glyphicon glyphicon-fire"></span> <span itemprop="name"><?php _e('最新文章'); ?></span></h3></div>
				<ul class="list-group">
					<?php $this->widget('Widget_Contents_Post_Recent')
					->parse('<li class="list-group-item"><a href="{permalink}">{title}</a></li>'); ?>
				</ul>
			</aside>
			<?php endif; ?>

			<?php if (!empty($this->options->sidebarBlock) && in_array('ShowRecentComments', $this->options->sidebarBlock)): ?>
			<aside class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title"><span class="glyphicon glyphicon-share"></span> <span itemprop="name"><?php _e('最近回复'); ?></span></h3></div>
				<ul class="list-group">
				<?php $this->widget('Widget_Comments_Recent')->to($comments); ?>
				<?php while($comments->next()): ?>
					<li class="list-group-item"><a href="<?php $comments->permalink(); ?>"><?php $comments->author(false); ?></a>: <?php $comments->excerpt(35, '...'); ?></li>
				<?php endwhile; ?>
				</ul>
			</aside>
			<?php endif; ?>

			<?php if (!empty($this->options->sidebarBlock) && in_array('ShowArchive', $this->options->sidebarBlock)): ?>
			<aside class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title"><span class="glyphicon glyphicon-book"></span> <span itemprop="name"><?php _e('归档'); ?></span></h3></div>
				<ul class="list-group">
					<?php $this->widget('Widget_Contents_Post_Date', 'type=month&format=F Y')
					->parse('<li class="list-group-item"><a href="{permalink}">{date}</a></li>'); ?>
				</ul>
			</aside>
			<?php endif; ?>

			<aside class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title"><span class="glyphicon glyphicon-cloud"></span> <span itemprop="name"><?php _e('标签云'); ?></span></h3></div>
				<div class="panel-body">
					<div class="nav-tag">
					<?php $this->widget('Widget_Metas_Tag_Cloud', array('sort' => 'count', 'ignoreZeroCount' => true, 'desc' => true, 'limit' => 20))->to($tags); ?>
					<?php while($tags->next()): ?>
					<a rel="tag" href="<?php $tags->permalink(); ?>"><?php $tags->name(); ?></a>
					<?php endwhile; ?>
					</div>
				</div>
			</aside>

			<aside class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title"><span class="glyphicon glyphicon-leaf"></span> <span itemprop="name"><?php _e('友情链接'); ?></span></h3></div>
				<ul class="list-group">
					<li class="list-group-item"><a href="http://blog.fomde.com" target="_blank"><?php _e('优质元素'); ?></a></li>
				</ul>
			</aside>

			<?php if (!empty($this->options->sidebarBlock) && in_array('ShowOther', $this->options->sidebarBlock)): ?>
			<aside class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title"><span class="glyphicon glyphicon-leaf"></span> <span itemprop="name"><?php _e('其它'); ?></span></h3></div>
				<ul class="list-group">
					<li class="list-group-item"><a href="<?php $this->options->feedUrl(); ?>"><?php _e('文章 RSS'); ?></a></li>
					<li class="list-group-item"><a href="<?php $this->options->commentsFeedUrl(); ?>"><?php _e('评论 RSS'); ?></a></li>
				</ul>
			</aside>
			<?php endif; ?>

		</div><!-- end #sidebar -->
	</div>