<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>

    <div id="goTop">
	<div class="arrow"></div>
	<div class="stick"></div>
    </div>
    <hr>
    <div class="footer-bottom">
      <ul class="list-inline text-center">
        <li>&copy; 2009-<?php echo date('Y'); ?> <a href="<?php $this->options->siteUrl(); ?>"><?php $this->options->title(); ?></a> / Design by <a href="http://www.fomde.com">Fomde</a> / Power by <a href="http://www.typecho.org" target="_blank">Typecho</a></li>
      </ul>
    </div>
</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://cdn.bootcss.com/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="http://cdn.bootcss.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <!-- Google Prettify -->
<script src="http://cdn.bootcss.com/prettify/r298/prettify.min.js"></script>
<script>
var pres=document.getElementsByTagName("pre");for(var i=0;i<pres.length;++i){pres[i].className="prettyprint linenums"}prettyPrint();$(document).ready(function(){$(window).scroll(function(){if($(this).scrollTop()>200){$("#goTop").fadeIn(200)}else{$("#goTop").fadeOut(200)}});$("#goTop").click(function(event){event.preventDefault();$("html, body").animate({scrollTop:0},300)})});
</script>
<!-- end Google Prettify -->
<?php $this->footer(); ?>
</body>
</html>