<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<?php $this->need('header.php'); ?>
<div class="col-md-9 col-sm-7" id="page">
	<div class="panel panel-default">
		<div class="panel-heading">
			<?php $this->title() ?>
		</div>
		<div class="panel-body">
			<span class="glyphicon glyphicon-calendar"></span> <time datetime="<?php $this->date('c'); ?>" itemprop="datePublished"><?php $this->date('F j, Y'); ?></time>&nbsp;&nbsp;&nbsp;&nbsp;
			<span class="glyphicon glyphicon-eye-open"></span><?php Views_Plugin::theViews(); ?> &nbsp;&nbsp;&nbsp;&nbsp;
			<span class="glyphicon glyphicon-edit"></span> <a href="<?php $this->permalink() ?>#comments"><?php $this->commentsNum('评论', '1 条评论', '%d 条评论'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-user"></span><a itemprop="name" href="<?php $this->author->permalink(); ?>" rel="author"><?php $this->author(); ?></a>
			<p><?php $this->content(); ?></p>
		</div>
	</div>
<?php $this->need('comments.php'); ?>
<?php $this->need('sidebar.php'); ?>
<?php $this->need('footer.php'); ?>
