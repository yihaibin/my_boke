<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>


<div id="column_1" class="SG_colW21 SG_colFirst">
<div class="SG_conn">
    <div class="SG_connHead">
    <span class="title">搜索</span>
    </div>
    <div class="SG_connBody">
    <div class="atcTitList relaList">
	<ul class="">
	<form id="search" method="post" value="输入关键字回车" action="./">
	<input  name="s" class="search" type="text" />
	</form>
    </ul>
    </div>
    </div>
</div>


<div class="SG_conn">
    <div class="SG_connHead">
    <span class="title">分类</span>
    </div>
    <div class="SG_connBody">
    <div class="atcTitList relaList">
	<ul class="">
  
	 <?php $this->widget('Widget_Metas_Category_List')
                ->parse('<li class="SG_j_linedot1"><p class="atcTitCell_tit SG_dot"><a href="{permalink}">{name}</a> <em class="count SG_txtb">({count})</em></p></li>'); ?>	 
		 
	    </ul>
  </div>
  </div>
</div>


<div class="SG_conn">
    <div class="SG_connHead">
    <span class="title">最新文章</span>
    </div>
    <div class="SG_connBody">
    <div class="atcTitList relaList">
	<ul class="">
     <?php $this->widget('Widget_Contents_Post_Recent')
            ->parse('<li class="SG_j_linedot1"><p class="atcTitCell_tit SG_dot"><a href="{permalink}">{title}</a></p></li>'); ?>
	    </ul>
  </div>
  </div>
</div>


<div class="SG_conn">
            <div class="SG_connHead">
            <span class="title">友情连接</span>
            </div>
            <div class="SG_connBody">
			<div class="zComments">
			<ul>
		 
			  <?php Links_Plugin::output(); ?>
				 
						</ul></div></div>
</div>
</div>
</div><!--/主体部分结束-->

 <!-- end #sidebar -->