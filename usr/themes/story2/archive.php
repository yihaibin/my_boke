<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<?php $this->need('header.php'); ?>


<!--主体部分开始-->
<div class="sinablogbody" id="sinablogbody"><div id="column_2" class="SG_colW73">
<div class="SG_conn">
	<div class="SG_connHead">
		<span class="title"><?php $this->archiveTitle(array(
            'category'  =>  _t('分类 %s 下的文章'),
            'search'    =>  _t('包含关键字 %s 的文章'),
            'tag'       =>  _t('标签 %s 下的文章'),
            'author'    =>  _t('%s 发布的文章')
        ), '', ''); ?></span>
	</div>
	<div class="SG_connBody" >
		<div class="bloglist">

       <?php if ($this->have()): ?>
    	<?php while($this->next()): ?>
						<div class="blog_title_h">
		<span class="img1"></span>
		<div class="blog_title">
		  <a href="<?php $this->permalink() ?>"><?php $this->title() ?></a>
		</div>
		<span class="time SG_txtc"><?php $this->date('F j, Y'); ?></span>
		</div>
					<div class="content"><?php $this->content(); ?>
<p class="readmore"><a href="<?php $this->permalink() ?>">阅读全文&gt;&gt;</a></p></div>	
						<div class="tagMore">
							<div class="tag SG_txtc">
								   		<?php $this->category(','); ?>
									┆ <a href="<?php $this->permalink() ?>">阅读</a>()
								┆ <a href="<?php $this->permalink() ?>#comments">评论</a>()
								┆ <a href="<?php $this->permalink() ?>">查看全文&raquo;</a>
								</div>
							</div>
						<div class="SG_j_linedot"></div>
	<?php endwhile; ?>
       <?php else: ?>
         <div class="blog_title_h">
                <h2 class="post-title"><?php _e('没有找到内容'); ?></h2>
            	</div>
						<div class="SG_j_linedot"></div>
        <?php endif; ?>
</div>
	</div>  
<div id="pagenavi">
    <?php $this->pageNav('&laquo; 前一页', '后一页 &raquo;'); ?>
</div>
</div>
</div>
<!-- end #main-->














	<?php $this->need('sidebar.php'); ?>
	<?php $this->need('footer.php'); ?>
