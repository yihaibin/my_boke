<?php
/**
 * 本模板为仿新浪博客皮肤的《故事2》主题
 * 
 * @package story2 Theme 
 * @author story2
 * @version 1.2
 * @link http://typecho.org
 */

if (!defined('__TYPECHO_ROOT_DIR__')) exit;
 $this->need('header.php');
 ?>
<!--主体部分开始-->
<div class="sinablogbody" id="sinablogbody"><div id="column_2" class="SG_colW73">
<div class="SG_conn">
	<div class="SG_connHead">
		<span class="title">我的博文</span>
	</div>
	<div class="SG_connBody" >
		<div class="bloglist">

	<?php while($this->next()): ?>
						<div class="blog_title_h">
		<span class="img1"></span>
		<div class="blog_title"><?php $this->content(); ?>
		  <a href="<?php $this->permalink() ?>"><?php $this->sticky(); $this->title() ?></a>
		</div>
		<span class="time SG_txtc"><?php $this->date('F j, Y'); ?></span>
		</div>
					<div class="content">
<p class="readmore"><a href="<?php $this->permalink() ?>">阅读全文&gt;&gt;</a></p></div>	
						<div class="tagMore">
							<div class="tag SG_txtc">
								   		<?php $this->category(','); ?>
									┆ <a href="<?php $this->permalink() ?>">阅读</a>(<?php $this->views(); ?>)
								┆ <a href="<?php $this->permalink() ?>#comments">评论</a>()
								┆ <a href="<?php $this->permalink() ?>">查看全文&raquo;</a>
								</div>
							</div>
						<div class="SG_j_linedot"></div>
	<?php endwhile; ?>
</div>
	</div>  
<div id="pagenavi">
    <?php $this->pageNav('&laquo; 前一页', '后一页 &raquo;'); ?>
</div>
</div>
</div>
<!-- end #main-->

<?php $this->need('sidebar.php'); ?>
<?php $this->need('footer.php'); ?>
