<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<?php $this->need('header.php'); ?>

<!--主体部分开始-->
<div class="sinablogbody" id="sinablogbody"><div id="column_2" class="SG_colW73">
<div class="SG_conn">
	<div class="SG_connHead">
		<span class="title">我的博文</span>
	</div>
	<div class="SG_connBody" >
		<div class="bloglist">
		<div class="blog_title_h">
		<span class="img1"></span>
		<div class="blog_title">
		 <?php $this->title() ?>		</div>
		<span class="time SG_txtc"><?php $this->date('F j, Y'); ?> </span>
		</div>
					<div class="content"><?php $this->content(); ?>

</div>
<!--分享开始-->
<div class="bshare-custom"><div class="bsPromo bsPromo1"></div><a title="分享到" href="http://www.bShare.cn/" id="bshare-shareto" class="bshare-more">分享到</a><a title="分享到QQ空间" class="bshare-qzone">QQ空间</a><a title="分享到新浪微博" class="bshare-sinaminiblog">新浪微博</a><a title="分享到人人网" class="bshare-renren">人人网</a><a title="分享到腾讯微博" class="bshare-qqmb">腾讯微博</a><a title="分享到网易微博" class="bshare-neteasemb">网易微博</a><a title="更多平台" class="bshare-more bshare-more-icon more-style-addthis"></a></div><script type="text/javascript" charset="utf-8" src="http://static.bshare.cn/b/button.js#style=-1&amp;uuid=&amp;pophcol=1&amp;lang=zh"></script><a class="bshareDiv" onclick="javascript:return false;"></a><script type="text/javascript" charset="utf-8" src="http://static.bshare.cn/b/bshareC0.js"></script>
<!--分享结束-->
<div class="tagMore">
							<div class="tag SG_txtc">
								   		<?php $this->category(','); ?>
									┆ 阅读(<?php $this->views(); ?>)
								┆ <a href="#comments">评论</a>(<?php $this->commentsNum(_t('0'), _t('1'), _t('%d')); ?>)
								</div>
							</div>
						<div class="SG_j_linedot">
						</div>
<div class="neighbor">
						前一篇： <?php $this->thePrev('%s','没有了'); ?>
				|
				 后一篇：<?php $this->theNext('%s','没有了'); ?>&raquo;
					</div>
</div>
			<div class="writeComm">
<?php $this->need('comments.php'); ?>
</div>
</div>
</div>
</div>



 
<?php $this->need('sidebar.php'); ?>
<?php $this->need('footer.php'); ?>
